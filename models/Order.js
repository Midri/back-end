var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OrderSchema = new Schema({
    id_order: {
        type: Schema.Types.ObjectId,
        auto: true,
    },
	customernumber: {
        type: Number
    },
    customernumber_client: {
        type: String,
    },
    id_exhibition: {
        type: String,
    },
    articles: {
        type: Array
    },
    
    status: {
        type: String,
        enum: [
            "Shipped",
            "awaiting storage",
            "Not processed Yet",
            "Hibernating",
            "Pending"
        ],
    },
    created_on: {
        type: Date,
    }


},

{ versionKey: false });

module.exports = Order = mongoose.model('Order', OrderSchema);
