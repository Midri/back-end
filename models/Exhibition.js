var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ExhibitionSchema = new Schema({

	id_exhibition: {
        type: Schema.Types.ObjectId,
        auto: true,
    },
    id_client: {
        type: Number
    },
    articles: [
        {
            id_article: {
                type: Number,
            },
            amount: {
                type: String,
            }
        }
    ],
    Status: {
        type: String,
        enum: [
            "Closed",
            "Ongoing",
            "Pending"
        ],
    },
    Created_on: {
        type: Date,
    },



},

{ versionKey: false })

module.exports = Exhibition = mongoose.model('Exhibition', ExhibitionSchema);