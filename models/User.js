var mongoose = require ('mongoose');
const Schema = mongoose.Schema;


const UserSchema = new Schema({
    id_client:{
        type: Number,
        auto: true,
    },
	customernumber: {
        type: Number
    },
    name: {
        type: String,
    },
    code: {
        type: String,
    },
    customernumber_client: {
        type: String,
    },
	articles:
        {
        type: Array
        }
    ,
    orders:{
        type: Array
    }
},
{ versionKey: false });

module.exports = User = mongoose.model('User', UserSchema);