var mongoose = require('mongoose');
const Schema = mongoose.Schema;
//create Schema

const ArticleSchema = new Schema({
	id_article: {
        type: Number,
        auto: true,
    },
    customernumber: {
        type: Number
    },
    id_client: {
        type: Number
    },
    client_article_counter:{ 
        type: Number
    },
    article_number: {
        type: String
    },
    article_number_external: {
        type: String
    },
    EAN: {
        type: String
    },
	name :{
		type: String,
	},
     description: {
        type: String
    },
     batch_tracking: {
        type: String,
        enum:[ "YES", "NO"]
    },
    unique_tracking: {
        type: String,
        enum:[ "YES", "NO"]
    },
    metrics_tracking: {
        type: String,
        enum:[ "YES", "NO"]
    },
    lhm: {
    	type: String,
    },
    length: {
        type: Number,
    },
	width: {
        type: Number
    },
    height: {
        type: Number
    },
    volume: {
        type: Number
    },
    weight: {
        type: String
    },
    length_max: {
        type: Number
    },
    width_max: {
        type: Number
    },
    height_max: {
        type: Number
    },
    volume_max: {
        type: String
    },
    weight_max: {
        type: String
    },
    trading_unit: {
        type: Number
    },
    purchase_price: {
        type: String
    },
    purchase_Currency: {
        type: String
    },
    customs_price: {
        type: String
    },
    customs_currency: {
        type: String
    },
    type: {
        type: String
    },
    Created: {
		type: Date
	},
    Updated_on: {
		type: Date
	},
   	storageArea: {
        type: String,
    },
    client_article_counter: {
        type: Number
    },
    
},
{ versionKey: false });

module.exports = Article = mongoose.model('Article', ArticleSchema)