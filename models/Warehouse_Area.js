var mongoose = require('mongoose');
const Schema = mongoose.Schema;


const WarehouseAreaSchema = new Schema({
	 Area_Id: {
        type: Schema.Types.ObjectId,
        auto: true,
    },
    Orders: {
        Order_id: {
            type: Schema.Types.ObjectId
        }
    }
},
{ versionKey: false });

module.exports = WarehouseArea = mongoose.model('WarehouseArea', WarehouseAreaSchema);