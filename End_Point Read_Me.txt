ReadME structure:

I-   End-point root
II-  Possible Routes
III- Awaiting Implementation
VI- Mongoose Schema
V- In case of issues
------------------------
I- End-point root:

This End point is run on http://ffcf0e5d.ngrok.io and now is run locally on Midri's computer


------------------------
II- Possible routes:

******Article (http://ffcf0e5d.ngrok.io/api/articles)
	Get : "/"    ==> get all Articles
	      "/:id" ==> get Article with identifier id (i.e fro id:5262146845984 link to get article is: http://ffcf0e5d.ngrok.io/api/articles/5262146845984)
	      "/:customerid ==> get Articles belonging to a customer
	Post: "/"    ==> save an Article; the article object is sent in the body of the request as JSON object. It must follow the SCHEMA structure
	Put : "/:id" ==> Update a database document; 
		WARNING !: should also respect the namings and structure of Schema (i.e to update amount body shoud be {"amount": 2})
	Delete: ":/id" ==> Delete Article with identifier id

******Order (http://ffcf0e5d.ngrok.io/api/order)

	Get : "/"    ==> get all Orders
	      "/:id" ==> get Order with identifier id 
	      "/:customerid ==> get Order belonging to a customer
	Post: "/"    ==> save an Order; -Same rules as in Article-
	Put : "/:id" ==> Update a database document; 
		WARNING !: should also respect the namings and structure of Schema (i.e to update amount body shoud be {"amount": 2})
	Delete: ":/id" ==> Delete Article with identifier id

-----------------------------------------
III- Awaiting Implementation:

Exhibition routes
Client Routes
Warehouse_Areas Routes.

----------------------------------------
VI- Schemas:
- Copy and paste in JAVASCRIPT Editor for better readability -


**********ARTICLE***********
const ArticleSchema = new Schema({
	id_article: {
        type: Number,
    },
    customernumber: {
        type: Number
    },
    id_client: {
        type: Number
    },
    client_article_counter:{ 
        type: Number
    },
    article_number: {
        type: String
    },
    article_number_external: {
        type: String
    },
    EAN: {
        type: String
    },
	name :{
		type: String,
	},
     description: {
        type: String
    },
     batch_tracking: {
        type: String,
        enum:[ "YES", "NO"]
    },
    unique_tracking: {
        type: String,
        enum:[ "YES", "NO"]
    },
    metrics_tracking: {
        type: String,
        enum:[ "YES", "NO"]
    },
    lhm: {
    	type: String,
    },
    length: {
        type: Number,
    },
	width: {
        type: Number
    },
    height: {
        type: Number
    },
    volume: {
        type: Number
    },
    weight: {
        type: String
    },
    length_max: {
        type: Number
    },
    width_max: {
        type: Number
    },
    height_max: {
        type: Number
    },
    volume_max: {
        type: String
    },
    weight_max: {
        type: String
    },
    trading_unit: {
        type: Number
    },
    purchase_price: {
        type: String
    },
    purchase_Currency: {
        type: String
    },
    customs_price: {
        type: String
    },
    customs_currency: {
        type: String
    },
    type: {
        type: String
    },
    Created: {
		type: Date
	},
    Changed: {
		type: Date
	},
   	storageArea: {
        type: String,
    },
    client_article_counter: {
        type: Number
    },
   
});

**********ORDER************
const OrderSchema = new Schema({
    id_order: {
        type: Schema.Types.ObjectId,
    },
	id_client: {
        type: Number
    },
    exhibition_id: {
        type: Schema.Types.ObjectId,
    },
    articles: {
        type: Array
    },
    
    Status: {
        type: String,
        enum: [
            "Shipped",
            "awaiting storage",
            "Not processed Yet",
            "Hibernating",
            "Pending"
        ],
    },
    Created_on: {
        type: Date,
    },


*********EXHIBITION***********
const ExhibitionSchema = new Schema({

	exhibition_id: {
        type: Schema.Types.ObjectId,
    },
    id_client: {
        type: Number
    },
    articles: [
        {
            id_article: {
                type: Number,
            },
            amount: {
                type: String,
            }
        }
    ],
    Status: {
        type: String,
        enum: [
            "Closed",
            "Ongoing",
            "Pending"
        ],
    },
    Created_on: {
        type: Date,
    },



})



**********WAREHOUSE_AREA***********
const WarehouseAreaSchema = new Schema({
	 Area_Id: {
        type: Schema.Types.ObjectId,
    },
    Orders: {
        Order_id: {
            type: Schema.Types.ObjectId
        }
    }
});


************USER**************

const UserSchema = new Schema({
    id_client:{
        type: Number,
    },
	customernumber: {
        type: Number
    },
    name: {
        type: String,
    },
    code: {
        type: String,
    },
    customernumber_client: {
        type: String,
    },
	articles:
        {
        type: Array
        }
    ,
    orders:{
        type: Array
    }
});


--------------------------------------------
V- In Case of Issue:

I am just next to you so... turn around and tell me what's wrong! 