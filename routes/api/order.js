const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');

//import Schema
const Order = require('../../models/Order');
router.use(bodyParser.json());


router.get('/', (req, res) => {
	Order.find()
	.sort({date: -1})
	.then(orders => res.json(orders));
});


router.get('/:id', (req, res) => {

	var ObjectId = require('mongoose').Types.ObjectId; 
	console.log(req.params.id)

	Order.find({_id: new ObjectId(req.params.id)})
		.then(order => res.send(order))
		.catch(err => res.status(404))
});

router.get('/:customerid/:id', (req, res) => {

	var ObjectId = require('mongoose').Types.ObjectId; 

	Order.find({id_order: new ObjectId(req.params.id), id_client: req.params.customerid})
		.then(order => res.send(order))
		.catch(err => res.status(404))
}); 

router.post('/', (req, res) => {

	
	req.body.status = "Pending",
	req.body.created_on = Date.now();
	console.log(req.body);

	//Assuming the order coming in the request already has the appropriate format
	const newOrder = new Order(req.body);
	//console.log(req.body);
	newOrder.save().then(order => res.send(order));
});

router.put('/:id', (req, res)=>{
	var ObjectId = require('mongoose').Types.ObjectId;

	req.body.Updated_on = Date.now();
	Order.findOneAndUpdate({_id: new ObjectId(req.params.id)}, req.body)
		.then(order => res.send(order))
		.catch(err => res.status(404))

});

router.delete('/:id', (req, res) => {
	var ObjectId = require('mongoose').Types.ObjectId;

		Order.findById(new ObjectId(req.params.id))
		 .then(order => order.remove().then(() => res.json({success:true})))
		 .catch(err => res.status(404).json({success: false}))

});


module.exports = router;