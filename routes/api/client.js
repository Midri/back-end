const express = require('express');

const router = express.Router();


const client = require('../../models/User');


router.get('/all', (req, res) => {

});

router.get('/:id', (req, res) => {

});

router.get('/:customer/:id', (req, res) => {

})

router.post('/', (req, res) => {
	const newUser = new User(req);
})

router.delete('/:customerid', (req, res) => {
	User.findById(req.params.customerid)
	//Delete Articles associated with this Client
	.then(user => 
		{
			(user.articles.id_article).forEachOrder(function(elem)
				{
					findById(elem).then(article => article.remove().then(()=>res.json({success:true})))
				})

		})
	//Delete Orders associated with that Client
	.then(user => 
		{
			(user.orders.id_order).forEachOrder(function(elem)
				{
					findById(elem).then(order => order.remove().then(()=>res.json({success:true})))
				})

		})
	})

module.exports = router;