const express = require('express');
const router = express.Router();

//article model
 const Article = require('../../models/Article');

//@route   GET api/article

//@desc    Get All Articles

//@acess   Public


//represents the /api/article calles in the server.js file
router.get('/', (req, res)=>{
	Article.find()
	.sort({date: -1})
	.then(articles => res.json(articles));

});

router.get('/:id', (req, res) => {

	var ObjectId = require('mongoose').Types.ObjectId; 
	console.log(req.params.id)

	Article.find({_id: new ObjectId(req.params.id)})
		.then(article => res.send(article))
		.catch(err => res.status(404))
});



router.get('/:customerid', (req, res) => {


	Article.find({id_client: req.params.customerid})
		.then(article => res.send(article))
		.catch(err => res.status(404))
});

//@route   POST api/article

//@desc    Create Article

//@acess   Public


//represents the /api/article calles in the server.js file
router.post('/', (req, res)=>{

	req.body.Updated_on = Date.now();
	req.body.created_on = Date.now();
	const newArticle = new Article(req.body);
	
	newArticle.save().then(article => res.json(article));
});

router.put('/:id', (req, res)=>{
	var ObjectId = require('mongoose').Types.ObjectId;

	req.body.Updated_on = Date.now();
	Article.findOneAndUpdate({_id: new ObjectId(req.params.id)}, req.body)
		.then(article => res.send(article))
		.catch(err => res.status(404))

});



//@route   DELETE api/article/:id

//@desc    Create Article

//@acess   Public


//represents the /api/article calles in the server.js file
router.delete('/:id', (req, res)=>{
		Article.findById(req.params.id)
		 .then(article => article.remove().then(() => res.json({success:true})))
		 .catch(err => res.status(404).json({success: false}))
});



 

module.exports = router;

