const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const articles = require('./routes/api/articles');
const orders = require('./routes/api/order');
const client = require('./routes/api/client');
const cors = require('cors');
const app = express();

//BodyParser Middleware

app.use(bodyParser.json());
app.use(cors());
const db = require('./config/keys').mongoURI;

//Connect to mongo


mongoose.connect(db, { useNewUrlParser: true })
.then(()=> console.log('MongoDB Connected...'))
.catch(err => console.log(err));

//Use Routes
app.get('/', (req, res)=>{
	res.send("Check Possible Routes");

});
app.use('/api/articles', articles);
app.use('/api/orders', orders);
app.use('/api/client', client);

//Port Listening
const port = process.env.PORT || 5000;

app.listen(port, ()=> console.log('server started on port', port)); 